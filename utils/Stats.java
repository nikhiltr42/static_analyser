package utils;

import java.util.HashMap;
import java.util.Map;

import ptg.EscapeStatus;
import ptg.ObjectNode;
import soot.SootMethod;

public class Stats {
	int noEscape;
	int escape;
	int total;
	double percentage;
	
	public Stats(HashMap<SootMethod,HashMap<ObjectNode,EscapeStatus>> summaries) {
		noEscape=0; escape=0; percentage=0;
		for(Map.Entry<SootMethod, HashMap<ObjectNode, EscapeStatus>> e : summaries.entrySet()) {
			for(Map.Entry<ObjectNode, EscapeStatus> ee : e.getValue().entrySet()) {
				if(ee.getValue().containsNoEscape()) noEscape++;
				else escape++;
			}
		}
		total = noEscape + escape;
		percentage = (noEscape*100.0/total);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Total Objects: "+total+" ");
		sb.append("NoEscape: "+noEscape+" ");
		sb.append(String.format("(%.2f%%) ", percentage));
		sb.append("Escape: "+escape+" ");
		sb.append(String.format("(%.2f%%) ", 100.0-percentage));
		return sb.toString();
	}
}
