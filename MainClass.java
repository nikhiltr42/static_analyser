
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map;

import analysers.StaticAnalyser;
import ptg.ObjectNode;
import ptg.PointsToGraph;
import utils.Stats;
import resolver.SummaryResolver;
import ptg.EscapeStatus;
import soot.PackManager;
import soot.SootMethod;
import soot.Transform;

import utils.GetListOfNoEscapeObjects;


public class MainClass {
	static HashMap<String, String> paths = new HashMap<>();
	public static void main(String[] args){
		/*
		 * args[0] -> class path 
		 * args[1] -> directory to be processed/analysed
		 * args[2] -> main class
		 * args[3] -> sootoutput directory
		 */
		
//		String classpath = ".:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/rt.jar:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/jce.jar";

		
		String[] sootArgs = {
//				"-whole-program",
				"-coffi",
				"-app",
				"-allow-phantom-refs",
				"-keep-bytecode-offset",
				"-keep-offset",
				"-soot-classpath", args[0], "-prepend-classpath",
				"-keep-line-number",
				"-process-dir", args[1],
				"-main-class", args[2],
				"-output-dir", args[3],
				"-output-format", "jimple"
			};
		
		
		/*
		String[] sootArgs = {
				// "-whole-program",
				"-coffi",
				"-app",
				 "-allow-phantom-refs",
				"-keep-bytecode-offset",
				"-keep-offset",
				"-soot-classpath", "/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/rt.jar:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/jce.jar:/home/nikhil/MTP/benchmarks/out:/home/nikhil/MTP/benchmarks/dacapo-9.12-MR1-bach.jar", "-prepend-classpath",
				"-keep-line-number",
				"-main-class", "Harness",
				"-process-dir", "/home/nikhil/MTP/benchmarks/out",
				"-p", "cg", "reflection-log:/home/nikhil/MTP/benchmarks/out/refl.log",
				"-output-dir", "/home/nikhil/MTP/soot_output_dir/dacapo",
				"-output-format", "jimple",
				"-include", "org.apache.",
				"-include", "org.w3c."
			};
		*/
		StaticAnalyser staticAnalyser = new StaticAnalyser();
		PackManager.v().getPack("jtp").add(new Transform("jtp.sample", staticAnalyser));
		soot.Main.main(sootArgs);
		System.out.println("Static Analysis is done!");
		
		
//		staticAnalyser.printAnalysis();
		
//		printResForJVM(StaticAnalyser.summaries, "/home/nikhil/MTP/benchmarks/out", "/home/nikhil/MTP/soot_output_dir/dacapo");
		printResForJVM(StaticAnalyser.summaries, args[1], args[3]);
		printAllInfo(StaticAnalyser.ptgs, StaticAnalyser.summaries, args[1], args[3]);
		SummaryResolver sr = new SummaryResolver();
		sr.resolve(staticAnalyser.summaries, staticAnalyser.ptgs);
//		System.out.println("Number of NoEscape in unresolved:"+getNumberOfNoEscape(StaticAnalyser.summaries));
//		System.out.println("Number of NoEscape in resolved:"+getNumberOfNoEscape(sr.solvedSummaries));
		System.out.print("Unresolved: ");
		System.out.println(new Stats(StaticAnalyser.summaries));
		System.out.print("Resolved: ");
		System.out.println(new Stats(sr.solvedSummaries));
	}
	
	private static String getNumberOfNoEscape(HashMap<SootMethod, HashMap<ObjectNode, EscapeStatus>> solvedSummaries) {
		int i=0;
		for(Map.Entry<SootMethod, HashMap<ObjectNode, EscapeStatus>> e : solvedSummaries.entrySet()) {
			for(Map.Entry<ObjectNode, EscapeStatus> ee : e.getValue().entrySet()) {
				if(ee.getValue().containsNoEscape()) i++;
			}
		}
		return new String(""+i);
	}

	private static void printAllInfo(HashMap<SootMethod, PointsToGraph> ptgs,
			HashMap<SootMethod, HashMap<ObjectNode, EscapeStatus>> summaries, String ipDir, String opDir) {
		
		Path p_opDir = Paths.get(opDir);
		ptgs.forEach((method,ptg) -> {
			Path p_opFile = Paths.get(p_opDir.toString() + "/" + method.getDeclaringClass().toString()+".info");
//			System.out.println("Method "+method.toString()+" appends to "+p_opFile);
			StringBuilder output = new StringBuilder();
			output.append(method.toString()+"\n");
			output.append("PTG:\n");
			output.append(ptg.toString());
			output.append("\nSummary\n");
			output.append(summaries.get(method).toString()+"\n");
			try {
				Files.write(p_opFile, output.toString().getBytes(StandardCharsets.UTF_8), 
						Files.exists(p_opFile) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);				
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("Unable to write info of "+method.toString()+" to file "+p_opFile.toString());
				e.printStackTrace();
			}
		});
	}

	static void printResForJVM(HashMap<SootMethod,HashMap<ObjectNode,EscapeStatus>> summaries, String ipDir, String opDir) {
		// Open File
		Path p_ipDir = Paths.get(ipDir);
		Path p_opDir = Paths.get(opDir);
		
		Path p_opFile = Paths.get(p_opDir.toString() + "/" + p_ipDir.getFileName() + ".res");
		
//		System.out.println(p_opFile);
		// String Builder
		StringBuilder sb = new StringBuilder();
		summaries.forEach((method, summary) ->{
			sb.append(method.getBytecodeSignature().toString());
			sb.append(GetListOfNoEscapeObjects.get(summary));
			sb.append("\n");
		});
		try {
			
			System.out.println("Trying to write to:"+p_opFile);
			Files.write(p_opFile, sb.toString().getBytes(StandardCharsets.UTF_8), 
					Files.exists(p_opFile) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);
			System.out.println("Unresolved results have been written.");
		} catch (IOException e) {
			System.out.println("There is an IO exception");
			e.printStackTrace();
		}
	}
}